# JEA and You:
## Or, Sane Safety is Seriously Splendid

---

# The Way Things Are

+++
## So You're In Ops
- Lots of RDP |
- Admin or Nothing |
- Often Domain Admin... |

+++
## NotOps Needs Something
- Tickets for Simple tasks Not-Ops Needs |
- Some Scripting |
- Less Remoting |

+++
## Craters Everywhere

- Technical Debt |
- Wasted Time |
- Risk Exposure |

---

# What Do?

_If only we had a way to build endpoints we could **selectively** grant access to..._

---

# JEA to the Rescue (?)

+++

### Just Enough Administration

> ...a security technology that helps organizations enforce information security by restricting IT administrative rights. JEA provides a practical, role-based approach to set up and automate restrictions for IT personnel, and reduces the risks associated with providing users full administrative rights.

+++

### Wat?

- Safely, Sanely give remote PowerShell access to some users for some tasks. |

---

## Time for Terms

+++

### Role:
> Definition of **what** _can_ be done in a JEA endpoint.
> Defined in a data file ending with `.psrc`
> Some metadata and definitions of visible functions, aliases, cmdlets, providers, etc.

+++

### Endpoint:
> Definition of **who** can do _what_ on a particular endpoint.
> Defined in a data file ending with `.pssc`
> Some metadata and definitions of roles, language mode, etc.

---

## Rolling Up a Role

+++

```powershell
@{

    # ID used to uniquely identify this document
    GUID             = '175310ba-577d-40b3-9a3e-d20dafe96faf'

    # Author of this document
    Author           = 'Michael T Lombardi'

    # Description of the functionality provided by these settings
    Description      = 'Limited Read-Only role enabling information gathering but no changes to the system.'

    # Company associated with this document
    CompanyName      = 'BarbarianKB'

    # Copyright statement for this document
    Copyright        = '(c) 2017 Michael T Lombardi. All rights reserved.'

    # Cmdlets to make visible when applied to a session
    VisibleCmdlets   = 'Get-*'

    # Functions to make visible when applied to a session
    VisibleFunctions = 'Get-*'
}
```

@[3-16](Metadata about the role)
@[18-22](Definition of available commands in the endpoint)

---

## Ending Up With an Endpoint


+++?code=source/Templates/ReadOnly.ps1&lang=powershell&title=Template for Defining an Endpoint

@[4-14](Metadata about the endpoint)
@[16-17](Definition of Session Type)
@[19-20](Where the Transcripts Go)
@[22-24](Virtual Account Settings)
@[26-30](Role Definitions)

---

# What Time is It?

+++?video=https://media.giphy.com/media/oVAVuZyt6Xbkk/giphy.mp4

---

## HMU

+ [Twitter](https://twitter.com/barbariankb)
+ [GitLab](https://gitlab.com/michaeltlombardi)
+ [GitHub](https://github.com/michaeltlombardi)
+ [LinkedIn](https://www.linkedin.com/in/michaeltlombardi/)
