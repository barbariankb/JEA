<#

#>
[cmdletbinding()]
Param (
    [ValidateSet('Test','Build','Clear')]
    [String]$Task
)

$ModuleName = 'BarbarianKB.JEA'
$OutputPath = "$PSScriptRoot\$ModuleName"

Function Clear-BuildOutput {
    If (Test-Path -Path $OutputPath) {
        Remove-Item -Path $OutputPath -Force -Recurse
    }
}

Function New-BuildOutput {
    Clear-BuildOutput
    $null = New-Item -Path $OutputPath -ItemType Directory -Force
    Copy-Item -Path Source\* -Destination $OutputPath -Recurse -Force
    Get-Module $OutputPath -ListAvailable
}

Switch ($Task) {
    'Clear' {
        Clear-BuildOutput
    }
    'Build' {
        New-BuildOutput
    }
    'Test' {
        # TODO
    }
}